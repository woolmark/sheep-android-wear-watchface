package org.bitbucket.woolmark.sheep.android.wear.watchface;

import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.DateFormat;
import android.widget.TextView;

public class MainActivity extends Activity {

    private static final String PREF_SHEEP = "sheep";

    private static final String KEY_SHEEP_NUMBER = "sheep_number";

    private Handler mTimeHandler;

    @Override
    protected void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.main_activity);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mTimeHandler == null) {
            mTimeHandler = new Handler(getMainLooper()) {
                public void handleMessage(final Message msg) {
                    updateTimeText();

                    if (mTimeHandler != null) {
                        mTimeHandler.sendEmptyMessageDelayed(0, 100);
                    }
                }
            };

            mTimeHandler.sendEmptyMessage(0);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        mTimeHandler = null;

        SheepView sheepView = (SheepView) findViewById(R.id.sheep_view);
        if (sheepView != null) {
            saveSheepCount(this, sheepView.getSheepCount());
        }
    }

    private void updateTimeText() {
        TextView timeText = (TextView) findViewById(R.id.time_text);
        if (timeText != null) {
            CharSequence time = DateFormat.format(getString(R.string.time_format), new Date());
            timeText.setText(time);
        }
    }

    private void saveSheepCount(final Context context, final int count) {

        SharedPreferences prefs = context.getSharedPreferences(PREF_SHEEP, Context.MODE_PRIVATE);
        if (prefs != null) {
            prefs.edit().putInt(KEY_SHEEP_NUMBER, count).commit();
        }

    }

}
